import React, { useState } from 'react'
import Head from 'next/head'

import Footer from '@/components/Layout/Footer'
import useInterface from '@/store/hooks/useInterface'
import Navigation from './Navigation'

import SideBar from './SideBar'
import { SunIcon, MoonIcon } from '@heroicons/react/24/solid'
function Layout({ children, title }) {
    const { show } = useInterface()
    const [backend, setBackend] = useState(false)
    const [dark, setDark] = useState(false)
    return (
        <div className='min-h-screen font-mont overflow-x-hidden relative'>
            <Head>
                <title>{title}</title>
                <meta name="description" content="liismaiil domain dashboard " />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <Navigation />
            <div id="dark-mode-toggle" onClick={() => setDark((dark) => !dark)}
                className="fixed top-24 right-0 inline-block w-12 cursor-pointer 
            rounded-l-xl 
            bg-zinc-800 dark:bg-zinc-200 text-zinc-200 dark:text-zinc-900 p-2 
            text-3xl">
                {!dark ? <SunIcon className="h-6 w-6 text-blue-500" /> :
                    <MoonIcon className="h-6 w-6 text-blue-500"/>
                }
            </div>

            {backend ?
                <div className="grid grid-cols-[300px_1fr] h-screen max-h-screen mt-20 ">
                    <SideBar />

                    <div className='bg-violet-300  min-h-screen'>
                        {children}
                    </div>
                </div> :
                <div className='bg-blue-50  min-h-screen flex flex-col items-center 
                justify-center d'>

                    {children}
                </div>
            }




            <div className=' w-full flex justify-center items-center '>
                <Footer />
            </div>
        </div >

    )
}

export default Layout