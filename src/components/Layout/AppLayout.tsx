import React from 'react'
import Head from 'next/head'

import Footer from '@/components/Layout/Footer'
import useInterface from '@/store/hooks/useInterface'
import Navigation from './Navigation'
function Layout({ children, title }) {
    const { show } = useInterface()
    console.log('layout')
    return (
        <div className='min-h-screen font-mont overflow-x-hidden '>
            <Head>
                <title>{title}</title>
                <meta name="description" content="liismaiil domain dashboard " />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <Navigation />
            <div className="grid grid-cols-[300px_1fr] mt-" >
                <div className="bg-slate-300" >
                    vertical nav
                </div>
                <div>
                    {children}
                </div>
            </div>
            <div className=' w-full flex justify-center items-center '>
                <Footer />
            </div>
        </div>

    )
}

export default Layout