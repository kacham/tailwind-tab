import React, { useEffect } from 'react'


//import Feed from '@/components/presentation/Feed'
import UploadModal from '@/components/presentation/UploadModal'
import connectMongoose from '@/lib/mongoose-db'
import AppLayout from '@/components/Layout'
import Presentation from '@/components/presentation'
import Organisations from '@/components/Layout/Organisations'
import Guests from '@/components/Layout/Guests'
import ViewerModel from '@/api/viewer/Viewer.model'
import ProductModel from '@/api/product/Product.model'
import { ViewerTypeData } from './api/viewer/viewer.types'
import { ProductTypeData } from './api/product/product.types'
import MapComponent from '@/components/maps/MapComponent'


const Home = ({ organisations, products }: { organisations: ViewerTypeData[], products: ProductTypeData[] }) => {
  useEffect(() => {
    window.scrollTo(0, 0)
  }, [])


  return (
    <AppLayout title=' liismaiil domains dashboard '>
      <div className="bg-gray-200 min-h-screen mt-20 w-full mx-28 " >
        <header>

          <Organisations organisations={organisations} />
        </header>

        <Guests guests={[]} />
        <MapComponent />
      </div>
    </AppLayout>
  )
}

export default Home

export async function getStaticProps() {
await connectMongoose()
  const organisations: ViewerTypeData[] = [];
  const docs = await ViewerModel.find({ status: { $in: ['ORGA', 'ADMIN', 'LIIS'] } }).exec()
  docs.forEach((doc: ViewerTypeData) => {
    const { _id, createdAt, updatedAt, login, email,
      organisation = null, website = null, coords, status, addressGeo,
      instagram = null, stripe_account_id = null, phone = null, avatar,
      walletId = null, bio = null, } = doc._doc


    /**_id, login, email, phone, instagram, website, avatar: { url }, coords: { lat, long }, addressGeo, bio */

    organisations.push({
      _id: _id.toString(),
      login, email, stripe_account_id,
      phone, bio,
      status,
      website, instagram, organisation, walletId,
      addressGeo: `${addressGeo}`,
      createdAt: JSON.stringify(createdAt),
      coords,
      avatar: `${avatar.url}`,
      updatedAt: JSON.stringify(updatedAt),

    });

  });

  const products: ProductTypeData[] = [];
  const productsArray = await ProductModel.find({ productStatus: { $in: ['LIIS', 'ORGA', 'FAMI'] } }).exec()
  productsArray.forEach((doc: ProductTypeData) => {

    products.push({
      title, description, price, productStatus, promo, promotedBy, stock, image, author
    });
  });

  return {
    props: {
      organisations,
      products
    },
    revalidate: 600
  }

}