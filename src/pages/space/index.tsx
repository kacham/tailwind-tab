
import React, { useEffect, useState } from 'react'
import AppLayout from '@/components/Layout'
import fs from 'fs'
import { parse } from 'csv-parse';
import { dirname } from 'path';
import { fileURLToPath } from 'url';

export default function Space({ soura }) {
  const [souraAffichable, setSoura] = useState('')
  console.log({ soura })
  return (
    <AppLayout title=' liismaiil space  '>
      <div id="spacePage" className='flex justify-center  w-full
  justify-items-center mt-20 bg-slate-600 h-screen ' >
        <div className="flex flex-col">
          <h2 className='text-gray-200  '> qq </h2>
        </div>
        <div className="  ">
          <div className="flex flex-col ">

            <h2 className='text-gray-50  block'> Your own Space </h2>
            <div className="flex ">

              <div className="flex">Progress</div>
              <div className="flex">Stats</div>
              <div className="flex">Progress</div>
              <div className="flex">Stats</div>
            </div>
          </div>
          <div className="flex flex-row">safhas</div>
          <div className="flex">Progress</div>
          <div className="flex">Stats</div>

        </div>

      </div>
    </AppLayout>

  )
}


export async function getStaticProps() {
  try {
    const __dirname = dirname(fileURLToPath(import.meta.url));
    const processFile = async () => {
      const recordsSet = new Set<string>();
      const parser = fs
        .createReadStream(`${__dirname}/quran.csv`)
        .pipe(parse({
          /* delimiter: ','  */
        }));
     await  parser.on('readable', function () {
        let record; while ((record = parser.read()) !== null) {
          recordsSet.add(record[3]);
        }
        return {
          props: {
            soura: recordsSet
          },
          revalidate: 600
        }
      })
     
      await parser.on('error', function (err) {
        console.error(err.message);
        return {
          props: {
            soura: []
          },
          error: err,
          revalidate: 600
        }
      })
    }
   return await processFile()
    
  } catch (error) {
    return {
      props: {
        soura: []
      },
      error,
      revalidate: 600
    }
  }

}